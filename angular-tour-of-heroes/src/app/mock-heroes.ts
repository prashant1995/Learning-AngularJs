import { Hero } from './hero'

export const HEROES: Hero[] = [
    { id:11, name:'Iron Man'},
    { id:12, name:'Captain America'},
    { id:13, name:'Hulk'},
    { id:14, name:'Thor'},
];